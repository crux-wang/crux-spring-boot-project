/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.grpc.server;

import io.grpc.Server;
import org.junit.Test;
import org.springframework.boot.autoconfigure.AutoConfigurations;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.Assert.*;

public class GrpcServerPropertiesTest {

    private ApplicationContextRunner contextRunner = new ApplicationContextRunner()
            .withConfiguration(AutoConfigurations.of(GrpcServerAutoConfiguration.class));

    @Test
    public void isEnabled() {
        contextRunner.withPropertyValues("grpc.server.enabled=false")
                .run(context -> {
                    assertThat(context).doesNotHaveBean(GrpcServerProperties.class);
                    assertThat(context).doesNotHaveBean(Server.class);
                    assertThat(context).doesNotHaveBean(GrpcServerBuilderConfigurer.class);
                    assertThat(context).doesNotHaveBean(GrpcServerRunner.class);
                });
        contextRunner.withPropertyValues("grpc.server.enabled=true")
                .run(context -> {
                    assertThat(context).hasSingleBean(GrpcServerProperties.class);
                    assertThat(context).hasSingleBean(GrpcServerBuilderConfigurer.class);
                    assertThat(context).hasSingleBean(GrpcServerRunner.class);
                    assertThat(context).hasSingleBean(Server.class);
                });
        contextRunner.run(context -> {
            assertThat(context).hasSingleBean(GrpcServerProperties.class);
            assertThat(context).hasSingleBean(GrpcServerBuilderConfigurer.class);
            assertThat(context).hasSingleBean(GrpcServerRunner.class);
            assertThat(context).hasSingleBean(Server.class);
            GrpcServerProperties properties = context.getBean(GrpcServerProperties.class);
            assertEquals(50051, properties.getPort());
            assertEquals( "",properties.getInProcessServerName());
        });
    }

    @Test
    public void getPort() {
        contextRunner
                .withPropertyValues("grpc.server.port=11111")
                .run(context -> {
            GrpcServerProperties properties = context.getBean(GrpcServerProperties.class);
            assertEquals(11111, properties.getPort());
        });
    }

    @Test
    public void getInProcessServerName() {
        contextRunner
                .withPropertyValues("grpc.server.in-process-server-name=grpc-process")
                .run(context -> {
                    GrpcServerProperties properties = context.getBean(GrpcServerProperties.class);
                    assertEquals( "grpc-process",properties.getInProcessServerName());
                });
    }

    @Test
    public void isEnableReflection() {
        contextRunner
                .withPropertyValues("grpc.server.enable-reflection=true")
                .run(context -> {
                    GrpcServerProperties properties = context.getBean(GrpcServerProperties.class);
                    assertTrue( properties.isEnableReflection());
                });
    }

}