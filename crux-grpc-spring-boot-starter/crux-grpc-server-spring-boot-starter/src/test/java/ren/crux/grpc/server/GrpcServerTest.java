/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.grpc.server;


import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.ServerBuilder;
import io.grpc.examples.helloworld.GreeterGrpc;
import io.grpc.examples.helloworld.HelloReply;
import io.grpc.examples.helloworld.HelloRequest;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import ren.crux.grpc.server.test.HelloWorldServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest
@EnableAutoConfiguration
@Import(GrpcServerTest.TestConfiguration.class)
public class GrpcServerTest {

    @Configuration
    public static class TestConfiguration {

        @Bean
        public GrpcServerBuilderConfigurer grpcServerBuilderConfigurer() {
            return new GrpcServerBuilderConfigurer() {
                @Override
                public void configure(ServerBuilder<?> builder) {
                    builder.addService(new HelloWorldServiceImpl());
                }
            };
        }
    }

    private ManagedChannel channel;

    @Before
    public void setup() {
        channel = ManagedChannelBuilder
                .forAddress("localhost", 50051)
                // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
                // needing certificates.
                .usePlaintext()
                .build();
    }

    @Test
    public void call() throws InterruptedException {
        GreeterGrpc.GreeterBlockingStub stub = GreeterGrpc.newBlockingStub(channel);
        HelloReply reply = stub.sayHello(HelloRequest.newBuilder().setName("test").build());
        System.out.println(reply);
        Assert.assertEquals("hello : test", reply.getMessage());
        reply = stub.sayHello(HelloRequest.newBuilder().setName("test").build());
        System.out.println(reply);
    }

    @After
    public void tearDown() {
        channel.shutdown();
    }

}
