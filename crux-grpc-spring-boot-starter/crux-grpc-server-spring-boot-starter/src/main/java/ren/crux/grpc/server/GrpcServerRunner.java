/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.grpc.server;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import io.grpc.Server;
import io.grpc.services.HealthStatusManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;

import javax.annotation.PreDestroy;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * @author wangzhihui
 */
@Slf4j
public class GrpcServerRunner implements ApplicationRunner {

    private final Server server;

    private final HealthStatusManager healthStatusManager;

    private final ApplicationContext applicationContext;

    private Executor executor = Executors.newSingleThreadExecutor(
            new ThreadFactoryBuilder().setDaemon(true).setNameFormat("grpc-server-block-thread-%d").build());

    public GrpcServerRunner(Server server, HealthStatusManager healthStatusManager, ApplicationContext applicationContext) {
        this.server = server;
        this.healthStatusManager = healthStatusManager;
        this.applicationContext = applicationContext;
    }

    private void blockUntilShutdown() {
        executor.execute(() -> {
            try {
                log.info("blocking thread started.");
                server.awaitTermination();
            } catch (InterruptedException e) {
                log.error("Grpc server stopped.", e);
            }
        });
    }

    @PreDestroy
    public void destroy() throws Exception {
        log.info("Grpc server shutdown...");
        server.getServices().forEach(service -> healthStatusManager.clearStatus(service.getServiceDescriptor().getName()));
        server.shutdown();
        log.info("Grpc server stopped.");
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("Grpc Server starting...");
        server.start();
        applicationContext.publishEvent(new GrpcServerInitializedEvent(server));
        log.info("gRPC Server started, listening on port {}.", server.getPort());
        blockUntilShutdown();
    }
}
