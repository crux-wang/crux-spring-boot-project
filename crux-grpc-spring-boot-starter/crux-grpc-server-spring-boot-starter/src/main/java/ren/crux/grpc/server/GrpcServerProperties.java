/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.grpc.server;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.util.SocketUtils;

import javax.annotation.PostConstruct;

/**
 * @author wangzhihui
 */
@Data
@ConfigurationProperties(prefix = "grpc.server")
public class GrpcServerProperties {

    private boolean enabled = true;

    private int port = 50051;

    private String inProcessServerName = "";

    private boolean enableReflection = false;

    @PostConstruct
    public void init() {
        if (port<0){
            port = SocketUtils.findAvailableTcpPort();
        }
    }
}
