/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.grpc.server;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.health.v1.HealthCheckResponse;
import io.grpc.inprocess.InProcessServerBuilder;
import io.grpc.protobuf.services.ProtoReflectionService;
import io.grpc.services.HealthStatusManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author wangzhihui
 */
@Slf4j
@Configuration
@EnableConfigurationProperties(GrpcServerProperties.class)
@ConditionalOnProperty(prefix = "grpc.server", name = "enabled", havingValue = "true", matchIfMissing = true)
public class GrpcServerAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public GrpcServerBuilderConfigurer grpcServerBuilderConfigurer() {
        return new GrpcServerBuilderConfigurer();
    }

    @Bean
    @ConditionalOnMissingBean
    public HealthStatusManager healthStatusManager() {
        return new HealthStatusManager();
    }

    @Bean
    @ConditionalOnMissingBean
    public Server gRPCServer(HealthStatusManager healthStatusManager, GrpcServerProperties properties, GrpcServerBuilderConfigurer configurer) {
        ServerBuilder<?> serverBuilder;
        if (StringUtils.isNotBlank(properties.getInProcessServerName())) {
            log.info("Used in-process server ( {} )", properties.getInProcessServerName());
            serverBuilder = InProcessServerBuilder.forName(properties.getInProcessServerName());
        } else {
            log.debug("Used default server ( port:{} )", properties.getInProcessServerName());
            serverBuilder = ServerBuilder.forPort(properties.getPort());
        }
        // Add health service
        serverBuilder.addService(healthStatusManager.getHealthService());
        if (properties.isEnableReflection()) {
            serverBuilder.addService(ProtoReflectionService.newInstance());
            log.info("Enabled 'ProtoReflectionService'");
        }
        configurer.configure(serverBuilder);
        Server server = serverBuilder.build();
        server.getServices().forEach(service->{
            String name = service.getServiceDescriptor().getName();
            healthStatusManager.setStatus(name, HealthCheckResponse.ServingStatus.SERVING);
            log.info("Service ( {} ) has been registered.", name);
        });
        if (server.getServices().isEmpty()) {
            log.warn("No service added.");
        }
        return server;
    }

    @Bean
    public GrpcServerRunner gRPCServerRunner(Server server, HealthStatusManager healthStatusManager, ApplicationContext applicationContext){
        return new GrpcServerRunner(server,healthStatusManager,applicationContext);
    }

}
