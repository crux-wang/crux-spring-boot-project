/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.grpc.client.channel.impl;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.util.CollectionUtils;
import ren.crux.grpc.client.GrpcClient;
import ren.crux.grpc.client.channel.GrpcChannelConfigurer;
import ren.crux.grpc.client.channel.GrpcChannelDefinition;
import ren.crux.grpc.client.channel.GrpcChannelDefinitionRegistry;
import ren.crux.grpc.client.channel.exception.IllegalGrpcChannelDefinitionException;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Default implementation of {@link GrpcChannelDefinitionRegistry}
 * Used {@link GrpcChannelConfigurer}
 *
 * @author wangzhihui
 */
@Slf4j
public class GrpcChannelDefinitionRegistryImpl implements GrpcChannelDefinitionRegistry {

    private ConcurrentHashMap<String, GrpcChannelDefinition> definitions = new ConcurrentHashMap<>();

    public GrpcChannelDefinitionRegistryImpl(List<GrpcChannelConfigurer> channelConfigurers) {
        if (!CollectionUtils.isEmpty(channelConfigurers)) {
            channelConfigurers.forEach(channelConfigurer -> {
                GrpcChannelDefinition definition = new GrpcChannelDefinition();
                channelConfigurer.configure(definition);
                try {
                    register(definition);
                } catch (IllegalGrpcChannelDefinitionException e) {
                    ExceptionUtils.rethrow(e);
                }
            });
        }
    }

    /**
     * Register channel definition
     *
     * @param definition Channel definition
     * @throws IllegalGrpcChannelDefinitionException when definition is invalid. {@link GrpcChannelDefinition#isValid()}
     */
    @Override
    public void register(@NonNull GrpcChannelDefinition definition) throws IllegalGrpcChannelDefinitionException {
        if (definition.isValid()) {
            log.info("Register definition : {}", definition);
            definitions.put(definition.getId(), definition);
        } else {
            throw new IllegalGrpcChannelDefinitionException(definition.getId());
        }
    }

    /**
     * Get channel definition
     *
     * @param id Channel id {@link GrpcClient#channelId()}
     * @return An Optional of  GrpcChannelDefinition
     */
    @Override
    public Optional<GrpcChannelDefinition> getDefinition(String id) {
        return Optional.ofNullable(definitions.get(id));
    }

}
