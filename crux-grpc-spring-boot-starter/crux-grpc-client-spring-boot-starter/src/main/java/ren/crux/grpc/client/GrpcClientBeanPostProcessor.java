/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.grpc.client;

import io.grpc.Channel;
import io.grpc.ClientInterceptor;
import io.grpc.stub.AbstractStub;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ReflectionUtils;
import ren.crux.grpc.client.channel.GrpcChannelFactory;
import ren.crux.grpc.client.channel.exception.NoSuchGrpcChannelDefinitionException;
import ren.crux.grpc.client.interceptor.GrpcGlobalClientInterceptorRegistry;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Searches for fields in beans that are annotated with {@link GrpcClient} and sets them.
 *
 * @author wangzhihui
 */
@Slf4j
public class GrpcClientBeanPostProcessor implements BeanPostProcessor {

    private final GrpcChannelFactory factory;
    private final GrpcGlobalClientInterceptorRegistry registry;
    private final ApplicationContext context;

    public GrpcClientBeanPostProcessor(ApplicationContext context, GrpcGlobalClientInterceptorRegistry registry, GrpcChannelFactory factory) {
        this.context = context;
        this.factory = factory;
        this.registry = registry;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Class<?> clazz = bean.getClass();
        do {
            for (final Field field : clazz.getDeclaredFields()) {
                final GrpcClient annotation = AnnotationUtils.getAnnotation(field, GrpcClient.class);
                Class<?> fieldType = field.getType();
                if (annotation != null && AbstractStub.class.isAssignableFrom(fieldType)) {
                    Class<? extends ClientInterceptor>[] interceptorClasses = annotation.interceptors();
                    List<ClientInterceptor> interceptors = new ArrayList<>(registry.getGlobalInterceptors());
                    for (Class<? extends ClientInterceptor> interceptorClass : interceptorClasses) {
                        interceptors.add(context.getBean(interceptorClass));
                    }
                    AnnotationAwareOrderComparator.sort(interceptors);
                    log.debug("Inject grpc client, class : {}, field : {}, interceptors : {}", clazz, fieldType, interceptors);
                    ReflectionUtils.makeAccessible(field);
                    try {
                        Channel channel = factory.createChannel(annotation.channelId(), interceptors);
                        final Constructor<?> constructor = ReflectionUtils.accessibleConstructor(fieldType, Channel.class);
                        final Object instance = constructor.newInstance(channel);
                        ReflectionUtils.setField(field, getTargetBean(bean), instance);
                    } catch (NoSuchGrpcChannelDefinitionException e) {
                        throw new BeanInitializationException("Inject grpc client fail, No such channels : " + annotation.channelId());
                    } catch (Exception e) {
                        throw new BeanInitializationException("Inject grpc client error", e);
                    }
                }
            }
            clazz = clazz.getSuperclass();
        } while (clazz != null);
        return bean;
    }

    private Object getTargetBean(final Object bean) throws Exception {
        Object target = bean;
        while (AopUtils.isAopProxy(target)) {
            target = ((Advised) target).getTargetSource().getTarget();
        }
        return target;
    }

}
