/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.grpc.client.channel;

import io.grpc.Channel;
import io.grpc.ManagedChannelBuilder;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import ren.crux.grpc.client.GrpcClient;

/**
 * A GrpcChannelDefinition describes a channel instance,
 * {@link #buildChannel()} used to create channel instances.
 *
 * @author wangzhihuin
 */
@Getter
@ToString
public class GrpcChannelDefinition {

    /**
     * Channel id
     * {@link GrpcClient#channelId()}
     */
    private String id;
    /**
     * Official default channel builder
     */
    private ManagedChannelBuilder managedChannelBuilder;
    /**
     * Custom channel builder
     */
    private GrpcChannelBuilder customChannelBuilder;

    public GrpcChannelDefinition id(String id) {
        this.id = id;
        return this;
    }

    public GrpcChannelDefinition managedChannel(ManagedChannelBuilder builder) {
        this.managedChannelBuilder = builder;
        return this;
    }

    public GrpcChannelDefinition customChannel(GrpcChannelBuilder builder) {
        this.customChannelBuilder = builder;
        return this;
    }

    public Channel buildChannel() {
        if (customChannelBuilder == null) {
            return managedChannelBuilder.build();
        } else {
            return customChannelBuilder.build();
        }
    }

    public boolean isValid() {
        return StringUtils.isNotBlank(id) && (customChannelBuilder != null || managedChannelBuilder != null);
    }
}
