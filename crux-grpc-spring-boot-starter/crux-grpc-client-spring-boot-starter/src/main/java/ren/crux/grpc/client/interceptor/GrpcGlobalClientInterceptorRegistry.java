/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.grpc.client.interceptor;

import io.grpc.ClientInterceptor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * @author wangzhihui
 */
@Slf4j
public class GrpcGlobalClientInterceptorRegistry {

    protected List<ClientInterceptor> globalInterceptors = new ArrayList<>();
    protected final ApplicationContext context;

    public GrpcGlobalClientInterceptorRegistry(ApplicationContext context) {
        this.context = context;
    }

    @PostConstruct
    private void init(){
        context.getBeansWithAnnotation(GrpcGlobalClientInterceptor.class).values().forEach(interceptor -> {
            if (interceptor instanceof ClientInterceptor) {
                register((ClientInterceptor) interceptor);
            } else {
                log.warn("Invalid grpc global client interceptor, not instance of ClientInterceptor : {}", interceptor);
            }
        });
    }

    public void register(@NonNull ClientInterceptor interceptor) {
        globalInterceptors.add(interceptor);
    }

    public List<ClientInterceptor> getGlobalInterceptors() {
        return globalInterceptors;
    }

}
