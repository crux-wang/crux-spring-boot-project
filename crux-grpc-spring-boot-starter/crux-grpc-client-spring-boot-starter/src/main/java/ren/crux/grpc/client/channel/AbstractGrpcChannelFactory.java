/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.grpc.client.channel;

import io.grpc.Channel;
import io.grpc.ClientInterceptor;
import io.grpc.ClientInterceptors;
import io.grpc.ManagedChannel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import ren.crux.grpc.client.channel.exception.NoSuchGrpcChannelDefinitionException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Save all {@link ManagedChannel} and close it when the application shutdown
 *
 * @author wangzhihui
 */
@Slf4j
public abstract class AbstractGrpcChannelFactory implements GrpcChannelFactory {

    private final GrpcChannelDefinitionRegistry registry;
    private final List<Channel> channels = new ArrayList<>();

    public AbstractGrpcChannelFactory(GrpcChannelDefinitionRegistry registry) {
        this.registry = registry;
    }

    /**
     * Create channel with interceptors
     *
     * @param id           Channel id {@link GrpcChannelDefinition#getId()}
     * @param interceptors Grpc client interceptors
     * @return Target channel instance
     * @throws NoSuchGrpcChannelDefinitionException when the channel definition not registered in {@link GrpcChannelDefinitionRegistry}
     */
    @Override
    public Channel createChannel(String id, List<ClientInterceptor> interceptors) throws NoSuchGrpcChannelDefinitionException {
        Optional<GrpcChannelDefinition> optional = registry.getDefinition(id);
        GrpcChannelDefinition definition = optional.orElseThrow(() -> new NoSuchGrpcChannelDefinitionException(id));
        Channel channel = definition.buildChannel();
        if (channel instanceof ManagedChannel) {
            channels.add(channel);
        }
        if (!CollectionUtils.isEmpty(interceptors)) {
            channel = ClientInterceptors.interceptForward(channel, interceptors);
        }
        return channel;
    }

    /**
     * Close all managed channels
     */
    @Override
    public void close() {
        log.info("shutdown all managed channels...");
        channels.forEach(channel -> ((ManagedChannel) channel).shutdown());
    }
}
