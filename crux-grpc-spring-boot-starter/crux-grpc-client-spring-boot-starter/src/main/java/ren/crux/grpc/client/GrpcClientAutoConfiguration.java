/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.grpc.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ren.crux.grpc.client.channel.*;
import ren.crux.grpc.client.channel.impl.GrpcChannelFactoryImpl;
import ren.crux.grpc.client.channel.impl.GrpcChannelDefinitionRegistryImpl;
import ren.crux.grpc.client.interceptor.GrpcGlobalClientInterceptorRegistry;

import java.util.List;


/**
 * @author wangzhihui
 */
@Slf4j
@Configuration
@EnableConfigurationProperties(GrpcClientProperties.class)
public class GrpcClientAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    @Autowired(required = false)
    public GrpcChannelDefinitionRegistry gRPCChannelRegistry(List<GrpcChannelConfigurer> channelConfigurers) {
        return new GrpcChannelDefinitionRegistryImpl(channelConfigurers);
    }

    @Bean
    @ConditionalOnMissingBean
    public GrpcGlobalClientInterceptorRegistry gRPCGlobalClientInterceptorRegistry(ApplicationContext context) {
        return new GrpcGlobalClientInterceptorRegistry(context);
    }

    @Bean
    @ConditionalOnMissingBean
    public GrpcChannelFactory gRPCChannelFactory(GrpcChannelDefinitionRegistry registry) {
        return new GrpcChannelFactoryImpl(registry);
    }

    @Bean
    @ConditionalOnProperty(prefix = "grpc.client", name = "enabled-inject", havingValue = "true", matchIfMissing = true)
    public GrpcClientBeanPostProcessor grpcClientBeanPostProcessor(ApplicationContext context, GrpcChannelFactory factory, GrpcGlobalClientInterceptorRegistry registry) {
        return new GrpcClientBeanPostProcessor(context, registry, factory);
    }

}
