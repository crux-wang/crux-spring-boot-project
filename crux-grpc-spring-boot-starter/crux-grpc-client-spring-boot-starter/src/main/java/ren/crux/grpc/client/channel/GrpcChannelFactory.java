/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.grpc.client.channel;

import io.grpc.Channel;
import io.grpc.ClientInterceptor;
import ren.crux.grpc.client.channel.exception.NoSuchGrpcChannelDefinitionException;

import java.util.Collections;
import java.util.List;

/**
 * This factory creates grpc {@link Channel}s
 *
 * @author wangzhihui
 */
public interface GrpcChannelFactory extends AutoCloseable {

    /**
     * Create channel with interceptors
     *
     * @param id           Channel id {@link GrpcChannelDefinition#getId()}
     * @param interceptors Grpc client interceptors
     * @return Target channel instance
     * @throws NoSuchGrpcChannelDefinitionException when the channel definition not registered in {@link GrpcChannelDefinitionRegistry}
     */
    Channel createChannel(String id, List<ClientInterceptor> interceptors) throws NoSuchGrpcChannelDefinitionException;

    /**
     * Create channel without interceptors
     *
     * @param id Channel id {@link GrpcChannelDefinition#getId()}
     * @return Target channel instance
     * @throws NoSuchGrpcChannelDefinitionException when the channel definition not registered in {@link GrpcChannelDefinitionRegistry}
     */
    default Channel createChannel(final String id) throws NoSuchGrpcChannelDefinitionException {
        return createChannel(id, Collections.emptyList());
    }

}
