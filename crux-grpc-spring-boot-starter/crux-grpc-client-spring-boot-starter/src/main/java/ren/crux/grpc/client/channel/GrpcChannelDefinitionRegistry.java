/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.grpc.client.channel;

import lombok.NonNull;
import ren.crux.grpc.client.GrpcClient;
import ren.crux.grpc.client.channel.exception.IllegalGrpcChannelDefinitionException;

import java.util.Optional;

/**
 * All channels are registered here, It is used to create channel instances
 * <p>
 * {@link GrpcChannelFactory}
 * {@link GrpcChannelDefinition}
 *
 * @author wangzhihui
 */
public interface GrpcChannelDefinitionRegistry {

    /**
     * Register channel definition
     *
     * @param definition Channel definition
     * @throws IllegalGrpcChannelDefinitionException when definition is invalid. {@link GrpcChannelDefinition#isValid()}
     */
    void register(@NonNull GrpcChannelDefinition definition) throws IllegalGrpcChannelDefinitionException;

    /**
     * Get channel definition
     *
     * @param id Channel id {@link GrpcClient#channelId()}
     * @return An Optional of  GrpcChannelDefinition
     */
    Optional<GrpcChannelDefinition> getDefinition(String id);

}