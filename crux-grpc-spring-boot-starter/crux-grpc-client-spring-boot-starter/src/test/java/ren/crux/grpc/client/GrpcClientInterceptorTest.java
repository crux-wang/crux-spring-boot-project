/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.grpc.client;


import io.grpc.*;
import io.grpc.examples.helloworld.GreeterGrpc;
import io.grpc.examples.helloworld.HelloReply;
import io.grpc.examples.helloworld.HelloRequest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.Order;
import org.springframework.test.context.junit4.SpringRunner;
import ren.crux.grpc.client.channel.GrpcChannelConfigurer;
import ren.crux.grpc.client.channel.GrpcChannelFactory;
import ren.crux.grpc.client.channel.exception.NoSuchGrpcChannelDefinitionException;
import ren.crux.grpc.client.interceptor.GrpcClientInterceptor;

import java.util.Map;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@EnableAutoConfiguration
@Import(GrpcClientInterceptorTest.TestConfiguration.class)
public class GrpcClientInterceptorTest extends GrpcClientTestBase {

    public static class TestService {
        @GrpcClient(channelId = "default", interceptors = {TestConfiguration.AInterceptor.class, TestConfiguration.BInterceptor.class})
        public GreeterGrpc.GreeterBlockingStub stub;
    }

    private static final Metadata.Key<String> CUSTOM_HEADER_KEY =
            Metadata.Key.of("custom_client_header_key", Metadata.ASCII_STRING_MARSHALLER);

    private static abstract class BaseInterceptor implements ClientInterceptor {

        protected final String name;

        protected BaseInterceptor(String name) {
            this.name = name;
        }

        @Override
        public <ReqT, RespT> ClientCall<ReqT, RespT> interceptCall(MethodDescriptor<ReqT, RespT> method, CallOptions callOptions, Channel next) {
            return new ForwardingClientCall.SimpleForwardingClientCall<ReqT, RespT>(next.newCall(method, callOptions)) {

                @Override
                public void start(Listener<RespT> responseListener, Metadata headers) {
                    log.info("name : {}, headers : {}", name, headers);
                    /* put custom header */
                    headers.put(CUSTOM_HEADER_KEY, name);
                    super.start(responseListener, headers);
                }
            };
        }
    }

    @Configuration
    public static class TestConfiguration {

        @Bean
        public GrpcChannelConfigurer defaultConfig() {
            return definition -> definition.id("default")
                    .managedChannel(ManagedChannelBuilder
                            .forAddress("localhost", port)
                            .usePlaintext());
        }

        @Bean
        public TestService testService() {
            return new TestService();
        }

        @Order(2)
        @GrpcClientInterceptor
        public static class AInterceptor extends BaseInterceptor {
            protected AInterceptor() {
                super("A");
            }

            @Override
            public <ReqT, RespT> ClientCall<ReqT, RespT> interceptCall(MethodDescriptor<ReqT, RespT> method, CallOptions callOptions, Channel next) {
                return new ForwardingClientCall.SimpleForwardingClientCall<ReqT, RespT>(next.newCall(method, callOptions)) {
                    @Override
                    public void start(Listener<RespT> responseListener, Metadata headers) {
                        log.info("name : {}, headers : {}", name, headers);
                        Assert.assertEquals("B", headers.get(CUSTOM_HEADER_KEY));
                        super.start(responseListener, headers);
                    }
                };
            }
        }

        @Order(1)
        @GrpcClientInterceptor
        public static class BInterceptor extends BaseInterceptor {
            protected BInterceptor() {
                super("B");
            }
        }
    }

    @Autowired
    private GrpcChannelFactory factory;

    @Autowired
    private TestService testService;

    @Test
    public void call() throws NoSuchGrpcChannelDefinitionException {
        Map<String, GrpcChannelConfigurer> configurers = context.getBeansOfType(GrpcChannelConfigurer.class);
        Assert.assertEquals(1, configurers.size());
        Assert.assertNotNull(factory.createChannel("default"));
        Assert.assertNotNull(testService.stub);
        HelloReply reply = testService.stub.sayHello(HelloRequest.newBuilder().setName("test").build());
        Assert.assertNotNull(reply);
        Assert.assertEquals("hello : test", reply.getMessage());
    }
}
