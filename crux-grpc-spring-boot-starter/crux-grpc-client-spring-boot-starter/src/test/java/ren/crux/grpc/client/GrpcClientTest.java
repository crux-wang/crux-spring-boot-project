/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.grpc.client;


import io.grpc.ManagedChannelBuilder;
import io.grpc.examples.helloworld.GreeterGrpc;
import io.grpc.examples.helloworld.HelloReply;
import io.grpc.examples.helloworld.HelloRequest;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import ren.crux.grpc.client.channel.GrpcChannelConfigurer;
import ren.crux.grpc.client.channel.GrpcChannelFactory;
import ren.crux.grpc.client.channel.exception.NoSuchGrpcChannelDefinitionException;

import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@EnableAutoConfiguration
@Import(GrpcClientTest.TestConfiguration.class)
public class GrpcClientTest extends GrpcClientTestBase {

    public static class TestService {
        @GrpcClient("default")
        public GreeterGrpc.GreeterBlockingStub stub;
    }

    @Configuration
    public static class TestConfiguration {

        @Bean
        public GrpcChannelConfigurer defaultConfig() {
            return definition -> definition.id("default")
                    .managedChannel(ManagedChannelBuilder
                            .forAddress("localhost", port)
                            // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
                            // needing certificates.
                            .usePlaintext());
        }

        @Bean
        public GrpcChannelConfigurer devConfig() {
            return definition -> definition.id("dev")
                    .managedChannel(ManagedChannelBuilder
                            .forAddress("localhost", port)
                            // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
                            // needing certificates.
                            .usePlaintext());
        }

        @Bean
        public TestService testService() {
            return new TestService();
        }
    }

    @Autowired
    private GrpcChannelFactory factory;

    @Autowired
    private TestService testService;

    @Test
    public void call() throws NoSuchGrpcChannelDefinitionException {
        Map<String, GrpcChannelConfigurer> configurers = context.getBeansOfType(GrpcChannelConfigurer.class);
        Assert.assertEquals(2, configurers.size());
        Assert.assertNotNull(factory.createChannel("dev"));
        Assert.assertNotNull(factory.createChannel("default"));
        Assert.assertNotNull(testService.stub);
        HelloReply reply = testService.stub.sayHello(HelloRequest.newBuilder().setName("test").build());
        Assert.assertNotNull(reply);
        Assert.assertEquals("hello : test", reply.getMessage());
    }
}
