/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.grpc.client;


import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;
import ren.crux.grpc.client.test.HelloWorldServiceImpl;

import java.io.IOException;

public class GrpcClientTestBase {

    protected Server server;

    public static final int port = 50051;

    @Autowired
    protected ApplicationContext context;

    @Before
    public void setup() throws IOException {
        server = ServerBuilder.forPort(port).addService(new HelloWorldServiceImpl()).build();
        server.start();
    }

    @After
    public void tearDown() {
        if (server != null) {
            server.shutdown();
        }
    }
}
