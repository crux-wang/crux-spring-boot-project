/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.web;

import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import ren.crux.common.CommonConstants;
import ren.crux.web.interceptor.AnnotationInterceptorRegistrationConfigurer;

/**
 * @author wangzhihui
 */
@Configuration
@EnableConfigurationProperties(RestWebProperties.class)
@ComponentScan(basePackages = "ren.crux.web.interceptor")
public class RestWebAutoConfiguration {

    @Bean
    @ConditionalOnClass(Jackson2ObjectMapperBuilder.class)
    public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer() {
        return builder -> {
            // Conversion Long and Double type to String
            // In JavaScript, The max safe number is 9007199254740991 ( 2^53 ).
            // So, We need Convert.
            builder.serializerByType(Long.class, ToStringSerializer.instance)
                    .serializerByType(Long.TYPE, ToStringSerializer.instance)
                    .serializerByType(Double.class, ToStringSerializer.instance)
                    .serializerByType(Double.TYPE, ToStringSerializer.instance)
                    .modules(new GuavaModule())
                    // Default value used is UTC (NOT local timezone).
                    .timeZone(CommonConstants.GMT_8);
        };
    }

    @Bean
    @ConditionalOnMissingBean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public WebMvcConfigurer webMvcConfigurer(ApplicationContext context) {
        return new AnnotationInterceptorRegistrationConfigurer(context);
    }

}
