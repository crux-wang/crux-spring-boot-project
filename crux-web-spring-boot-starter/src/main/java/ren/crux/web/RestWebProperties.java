/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.web;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import ren.crux.web.common.RestWebResults;
import ren.crux.web.interceptor.RestExceptionHandlerAdvice;

import javax.annotation.PostConstruct;

/**
 * @author wangzhihui
 */
@Data
@ConfigurationProperties(prefix = "rest")
public class RestWebProperties {

    /**
     * {@link RestExceptionHandlerAdvice}
     */
    private boolean enableExceptionHandler = true;

    /**
     * always return http status ok ( 200 ) when is false
     */
    private boolean useOriginalHttpStatus = false;

    private boolean enableResponseAdvice = true;

    private int resultDefaultSuccessCode = RestWebResults.getDefaultSuccCode();
    private int resultDefaultFailureCode = RestWebResults.getDefaultFailCode();
    private String resultDefaultSuccessMsg = RestWebResults.getDefaultSuccMsg();
    private String resultDefaultFailureMsg = RestWebResults.getDefaultFailMsg();

    @PostConstruct
    public void init() {
        RestWebResults.setDefaultSuccCode(resultDefaultSuccessCode);
        RestWebResults.setDefaultFailCode(resultDefaultFailureCode);
        RestWebResults.setDefaultSuccMsg(resultDefaultSuccessMsg);
        RestWebResults.setDefaultFailMsg(resultDefaultFailureMsg);
    }

}
