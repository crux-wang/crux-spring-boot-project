/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.web.common;

import org.springframework.http.HttpStatus;

/**
 * Build tools of  {@link RestWebResultContainer}
 *
 * @author wangzhihui
 */
public class RestWebResults {

    private static int DEFAULT_FAIL_CODE = -1;
    private static int DEFAULT_SUCC_CODE = 0;
    private static String DEFAULT_SUCC_MSG = "ok";
    private static String DEFAULT_FAIL_MSG = "fail";

    private RestWebResults() {
    }

    public static RestWebResultContainer succeed(int code, String msg, Object data) {
        return new RestWebResultContainer(code, msg, data);
    }

    public static RestWebResultContainer succeed(String msg, Object data) {
        return succeed(DEFAULT_SUCC_CODE, msg, data);
    }

    public static RestWebResultContainer succeed(Object data) {
        return succeed(DEFAULT_SUCC_MSG, data);
    }

    public static RestWebResultContainer succeed() {
        return succeed(null);
    }

    public static RestWebResultContainer fail(int code, String msg, Object data) {
        return new RestWebResultContainer(code, msg, data);
    }

    public static RestWebResultContainer fail(String msg, Object data) {
        return fail(DEFAULT_SUCC_CODE, msg, data);
    }

    public static RestWebResultContainer fail(int code, String msg) {
        return fail(code, msg, null);
    }

    public static RestWebResultContainer fail(HttpStatus status, String msg, Object data) {
        return fail(status != null ? status.value() : DEFAULT_SUCC_CODE, msg, data);
    }

    public static RestWebResultContainer fail(HttpStatus status, String msg) {
        return fail(status.value(), msg);
    }

    public static RestWebResultContainer fail(HttpStatus status, Throwable e) {
        return fail(status.value(), e);
    }

    public static RestWebResultContainer fail(int code, Throwable e) {
        return fail(code, e.getMessage());
    }

    public static RestWebResultContainer fail(Throwable e) {
        return fail(DEFAULT_FAIL_CODE, e);
    }

    public static RestWebResultContainer fail(String msg) {
        return fail(DEFAULT_FAIL_CODE, msg, null);
    }

    public static RestWebResultContainer fail() {
        return fail(DEFAULT_FAIL_MSG);
    }

    public static RestWebResultContainer operation(boolean result) {
        return result ? succeed() : fail();
    }

    public static int getDefaultFailCode() {
        return DEFAULT_FAIL_CODE;
    }

    public static void setDefaultFailCode(int defaultFailCode) {
        DEFAULT_FAIL_CODE = defaultFailCode;
    }

    public static int getDefaultSuccCode() {
        return DEFAULT_SUCC_CODE;
    }

    public static void setDefaultSuccCode(int defaultSuccCode) {
        DEFAULT_SUCC_CODE = defaultSuccCode;
    }

    public static String getDefaultSuccMsg() {
        return DEFAULT_SUCC_MSG;
    }

    public static void setDefaultSuccMsg(String defaultSuccMsg) {
        DEFAULT_SUCC_MSG = defaultSuccMsg;
    }

    public static String getDefaultFailMsg() {
        return DEFAULT_FAIL_MSG;
    }

    public static void setDefaultFailMsg(String defaultFailMsg) {
        DEFAULT_FAIL_MSG = defaultFailMsg;
    }
}
