/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.web.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import ren.crux.web.RestWebProperties;
import ren.crux.web.common.RestWebResultContainer;
import ren.crux.web.common.RestWebResults;

/**
 * Restful Style Exception Handler
 *
 * @author wangzhihui
 */
@Slf4j
@RestControllerAdvice
@ConditionalOnProperty(prefix = "rest", name = "enable-exception-handler", havingValue = "true", matchIfMissing = true)
public class RestExceptionHandlerAdvice {

    private final RestWebProperties restWebProperties;

    public RestExceptionHandlerAdvice(RestWebProperties restWebProperties) {
        this.restWebProperties = restWebProperties;
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<RestWebResultContainer> httpRequestMethodNotSupported(HttpRequestMethodNotSupportedException exception, WebRequest request) {
        log.error("Request : {} catch exception : ", request, exception);
        return ResponseEntity
                .status(restWebProperties.isUseOriginalHttpStatus() ? HttpStatus.METHOD_NOT_ALLOWED : HttpStatus.OK)
                .body(RestWebResults.fail(HttpStatus.METHOD_NOT_ALLOWED, exception.getMessage()));
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<RestWebResultContainer> noHandlerFoundException(NoHandlerFoundException exception, WebRequest request) {
        log.error("Request : {} catch exception : ", request, exception);
        return ResponseEntity
                .status(restWebProperties.isUseOriginalHttpStatus() ? HttpStatus.NOT_FOUND : HttpStatus.OK)
                .body(RestWebResults.fail(HttpStatus.NOT_FOUND, exception.getMessage()));
    }


    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<RestWebResultContainer> missingServletRequestParameterException(MissingServletRequestParameterException exception, WebRequest request) {
        log.error("Request : {} catch exception : ", request, exception);
        return ResponseEntity
                .status(restWebProperties.isUseOriginalHttpStatus() ? HttpStatus.BAD_REQUEST : HttpStatus.OK)
                .body(RestWebResults.fail(HttpStatus.BAD_REQUEST, exception.getMessage()));
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<RestWebResultContainer> httpMessageNotReadableException(HttpMessageNotReadableException exception, WebRequest request) {
        log.error("Request : {} catch exception : ", request, exception);
        return ResponseEntity
                .status(restWebProperties.isUseOriginalHttpStatus() ? HttpStatus.BAD_REQUEST : HttpStatus.OK)
                .body(RestWebResults.fail(HttpStatus.BAD_REQUEST, exception.getMessage()));
    }

    @ExceptionHandler(InterceptException.class)
    public ResponseEntity<RestWebResultContainer> interceptException(InterceptException exception, WebRequest request) {
        log.error("Request : {} catch exception : ", request, exception);
        return ResponseEntity
                .status(restWebProperties.isUseOriginalHttpStatus() ? exception.getStatus() : HttpStatus.OK)
                .body(exception.getResult() instanceof RestWebResultContainer ?
                        (RestWebResultContainer) exception.getResult() : RestWebResults.fail(exception.getStatus(), RestWebResults.getDefaultFailMsg(), exception.getResult()));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<RestWebResultContainer> exception(Exception exception, WebRequest request) {
        log.error("Request : {} catch exception : ", request, exception);
        return ResponseEntity
                .status(restWebProperties.isUseOriginalHttpStatus() ? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.OK)
                .body(RestWebResults.fail(HttpStatus.INTERNAL_SERVER_ERROR, exception));
    }
}
