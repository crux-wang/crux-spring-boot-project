/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.web.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * Intercept with required annotation
 * <p>
 * See {@link InterceptException}
 *
 * @author wangzhihui
 */
@Slf4j
public abstract class AbstractAnnotationHandlerInterceptorAdapter<T extends Annotation> extends HandlerInterceptorAdapter {

    private static final String ANNOTATION_ATTRIBUTE_NAME = "__Annotation";
    private static final String ANNOTATION_CLASS_ATTRIBUTE_NAME = "__AnnotationClass";

    /**
     * Implement and return required annotation class
     *
     * @return
     */
    protected abstract Class<T> getRequiredAnnotationClass();

    protected boolean matchRequiredAnnotation(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.trace("Check required annotation start ...");
        if (getRequiredAnnotationClass() != null) {
            if (handler.getClass().isAssignableFrom(HandlerMethod.class)) {
                HandlerMethod handlerMethod = (HandlerMethod) handler;
                Method method = handlerMethod.getMethod();
                Annotation annotation = AnnotationUtils.findAnnotation(method, getRequiredAnnotationClass());
                if (annotation == null) {
                    annotation = AnnotationUtils.findAnnotation(method.getDeclaringClass(), getRequiredAnnotationClass());
                }
                if (annotation != null) {
                    request.setAttribute(ANNOTATION_ATTRIBUTE_NAME, annotation);
                    request.setAttribute(ANNOTATION_CLASS_ATTRIBUTE_NAME, getRequiredAnnotationClass());
                    log.trace("Match required annotation : {}", annotation);
                    return true;
                }
            }
        }
        return false;
    }

    protected Object before(HttpServletRequest request, HttpServletResponse response, Object handler, T anno) throws Exception {
        return true;
    }

    protected void after(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView, T anno) throws Exception {
    }


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (matchRequiredAnnotation(request, response, handler)) {
            Object result = before(request, response, handler, (T) request.getAttribute(ANNOTATION_ATTRIBUTE_NAME));
            if (result != Boolean.TRUE) {
                throw new InterceptException().withData(result);
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        if (getRequiredAnnotationClass() != null && getRequiredAnnotationClass() == request.getAttribute(ANNOTATION_CLASS_ATTRIBUTE_NAME)) {
            this.after(request, response, handler, modelAndView, (T) request.getAttribute(ANNOTATION_ATTRIBUTE_NAME));
            request.removeAttribute(ANNOTATION_ATTRIBUTE_NAME);
            request.removeAttribute(ANNOTATION_CLASS_ATTRIBUTE_NAME);
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        super.afterCompletion(request, response, handler, ex);
    }

    @Override
    public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        super.afterConcurrentHandlingStarted(request, response, handler);
    }
}
