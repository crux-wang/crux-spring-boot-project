/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.web.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Map;

/**
 * Configuration annotation interceptors
 *
 * @author wangzhihui
 */
@Slf4j
public class AnnotationInterceptorRegistrationConfigurer implements WebMvcConfigurer {

    private final ApplicationContext context;

    public AnnotationInterceptorRegistrationConfigurer(ApplicationContext context) {
        this.context = context;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        Map<String, Object> beans = context.getBeansWithAnnotation(Interceptor.class);
        for (Object bean : beans.values()) {
            Class<?> cls = bean.getClass();
            Interceptor annotation = cls.getAnnotation(Interceptor.class);
            if (annotation != null) {
                log.trace("Catch annotation interceptor : {}", cls);
                if (!(bean instanceof HandlerInterceptor)) {
                    log.warn("Invalid annotation interceptor : {},  Not implement org.springframework.web.servlet.HandlerInterceptor", cls);
                    continue;
                }
                InterceptorRegistration registration = registry.addInterceptor((HandlerInterceptor) bean);
                Order order = cls.getAnnotation(Order.class);
                if (order != null) {
                    registration.order(order.value());
                }
                if (annotation.value().length > 0) {
                    registration.addPathPatterns(annotation.value());
                }
                if (annotation.exclude().length > 0) {
                    registration.excludePathPatterns(annotation.exclude());
                }
                log.debug("add annotation interceptor [ {} ], order : {}, includePatterns : {}, excludePatterns : {}", cls, order != null ? order.value() : "null", annotation.value(), annotation.exclude());
            }
        }
    }

}
