/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.web.interceptor;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.WebRequest;
import ren.crux.web.common.RestWebResultContainer;
import ren.crux.web.common.RestWebResults;

/**
 * When http request  intercepted and expect to return an object, throw it.
 * It will be handled in {@link RestExceptionHandlerAdvice#interceptException(InterceptException, WebRequest)}.
 * <p>
 * Usually, you need to serialize the object and respond. But it may be differ from global processing.
 * Throwing {@link InterceptException} makes it easier.
 *
 * @author wangzhihui
 */
@Getter
public class InterceptException extends Exception {

    private HttpStatus status;
    private Object result;

    public InterceptException(HttpStatus status, Object result) {
        this.status = status;
        this.result = result;
    }

    public InterceptException(String msg) {
        super(msg);
        this.result = RestWebResults.fail(msg);
    }

    public InterceptException() {
        this.result = RestWebResults.fail();
    }

    public InterceptException status(HttpStatus status) {
        this.status = status;
        return this;
    }

    public InterceptException withMessage(String msg) {
        if (this.result == null) {
            this.result = RestWebResults.fail(msg);
        } else if (result instanceof RestWebResultContainer) {
            ((RestWebResultContainer) this.result).setMsg(msg);
        } else {
            this.result = RestWebResults.fail(msg, result);
        }
        return this;
    }

    public InterceptException withData(Object data) {
        if (data instanceof RestWebResultContainer) {
            this.result = data;
        } else {
            if (result instanceof RestWebResultContainer) {
                ((RestWebResultContainer) this.result).setData(data);
            } else {
                this.result = RestWebResults.fail(RestWebResults.getDefaultFailMsg(), data);
            }
        }
        return this;
    }

}
