/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.web.test;

import org.apache.commons.lang3.StringUtils;
import ren.crux.web.interceptor.AbstractAnnotationHandlerInterceptorAdapter;
import ren.crux.web.interceptor.Interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Interceptor("/foo/wrap/**")
public class TestAnnotationHandlerInterceptor extends AbstractAnnotationHandlerInterceptorAdapter<TestInterceptor> {

    @Override
    protected Class<TestInterceptor> getRequiredAnnotationClass() {
        return TestInterceptor.class;
    }

    @Override
    protected Object before(HttpServletRequest request, HttpServletResponse response, Object handler, TestInterceptor anno) throws Exception {
        if (StringUtils.equals(request.getParameter("param"), anno.value())) {
            return true;
        } else {
            return "fail-data";
        }
    }
}
