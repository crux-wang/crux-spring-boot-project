/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.web.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.NoHandlerFoundException;
import ren.crux.web.common.RestWebResults;

import java.util.Collections;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@SpringBootTest
@ComponentScan(basePackages = "ren.crux.web.test")
public class AnnotationInterceptorRegistrationConfigurerTest {

    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private ApplicationContext applicationContext;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(this.wac)
                .addDispatcherServletCustomizer(dispatcherServlet -> dispatcherServlet.setThrowExceptionIfNoHandlerFound(true))
                .alwaysDo(print()).build();
    }

    @Test
    public void normal() throws Exception {
        mockMvc.perform(get("/foo/normal")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(objectMapper.writeValueAsString(RestWebResults.succeed("normal"))));
    }

    @Test
    public void bar() throws Exception {
        mockMvc.perform(get("/foo/bar")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(objectMapper.writeValueAsString(RestWebResults.fail("deny"))));
        mockMvc.perform(get("/foo/bar?param=test&num=2")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(objectMapper.writeValueAsString(RestWebResults.succeed("bar"))));
    }

    @Test
    public void wrap() throws Exception {
        Assert.assertNotNull(applicationContext.getBean(RestResponseBodyAdvice.class));
        mockMvc.perform(get("/foo/wrap")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(objectMapper.writeValueAsString(RestWebResults.fail(RestWebResults.getDefaultFailCode(), RestWebResults.getDefaultFailMsg(), "fail-data"))));
        mockMvc.perform(get("/foo/wrap?param=test")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(objectMapper.writeValueAsString(RestWebResults.fail(RestWebResults.getDefaultFailCode(), RestWebResults.getDefaultFailMsg(), "fail-data"))));
        mockMvc.perform(get("/foo/wrap?param=val")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(objectMapper.writeValueAsString(RestWebResults.succeed(Collections.singleton("wrap")))));
    }

    @Test
    public void exception() throws Exception {
        // 500
        mockMvc.perform(get("/foo/exception")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(objectMapper.writeValueAsString(RestWebResults.fail(HttpStatus.INTERNAL_SERVER_ERROR, new Exception("test")))));
        // 405
        mockMvc.perform(post("/foo/exception")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(objectMapper.writeValueAsString(RestWebResults.fail(HttpStatus.METHOD_NOT_ALLOWED, new HttpRequestMethodNotSupportedException(HttpMethod.POST.name())))));
        // 400
        mockMvc.perform(get("/foo/obj?param=test")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(objectMapper.writeValueAsString(RestWebResults.fail(HttpStatus.BAD_REQUEST, new MissingServletRequestParameterException("container", "RestWebResultContainer")))));
        // 404
        mockMvc.perform(get("/foo/not-found")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(objectMapper.writeValueAsString(RestWebResults.fail(HttpStatus.NOT_FOUND, new NoHandlerFoundException(HttpMethod.GET.name(), "/foo/not-found", new HttpHeaders())))));

    }
}