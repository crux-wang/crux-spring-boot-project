/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.web;

import org.junit.Test;
import org.springframework.boot.autoconfigure.AutoConfigurations;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import ren.crux.web.interceptor.RestExceptionHandlerAdvice;
import ren.crux.web.interceptor.RestResponseBodyAdvice;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class RestWebAutoConfigurationTest {
    private ApplicationContextRunner contextRunner = new ApplicationContextRunner()
            .withConfiguration(AutoConfigurations.of(RestWebAutoConfiguration.class));

    @Test
    public void configurationTest() {
        contextRunner.run(context -> {
            assertThat(context).hasSingleBean(RestWebProperties.class);
            assertThat(context).hasSingleBean(RestExceptionHandlerAdvice.class);
            // Jackson2ObjectMapperBuilderCustomizer is not single bean
            assertThat(context).hasBean("jackson2ObjectMapperBuilderCustomizer");
            assertThat(context).hasSingleBean(RestTemplate.class);
            assertThat(context).hasSingleBean(WebMvcConfigurer.class);
            assertThat(context).hasSingleBean(RestResponseBodyAdvice.class);
        });
    }
}