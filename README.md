# crux-spring-boot-project

## crux-web
>>>
* 此项目主要支持 REST 架构，提倡前后端分离
* 提供了统一的返回值格式（RestWebResultContainer）及统一的异常处理（RestExceptionHandlerAdvice）
* 提供拦截器使用注解自动扫描配置（@Interceptor),支持配置拦截路径及优先级
* 提供 Rose 项目中个人比较喜欢的（ControllerInterceptorAdapter）Spring MVC 实现方式（AbstractAnnotationHandlerInterceptorAdapter）
>>>


# 快速接入

## 1. 添加 maven 依赖

```xml
<dependency>
    <groupId>ren.crux</groupId>
    <artifactId>crux-web-spring-boot-starter</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>
```

## 2. 可选配置

### 2.1 发生 404 时返回自定义数据而不是默认页面

```
# 出现错误时, 直接抛出异常, 此处为了自定义处理 404 情况
spring.mvc.throw-exception-if-no-handler-found=true
# 不要为我们工程中的资源文件建立映射, 此处为了自定义处理 404 情况
spring.resources.add-mappings=false

```

### 2.2 是否使用原始 http status code
```
# 默认值为 flase,在任何情况下都返回 200, 详细的状态码在返回值中
# 当为 true 时,返回原始的状态码
rest.use-original-http-status=false

```

### 2.3 是否开启全局异常拦截
```
# 默认为 true, 即拦截所有异常并统一处理，
# 包括当注解拦截器拦截器（`AbstractAnnotationHandlerInterceptorAdapter`）中的请求被拦截时抛出的`InterceptException`返回值处理
rest.enable-exception-handler=true
```

### 2.4 是否开启统一返回值处理
```
# 默认为 true, 开启后会自动将 Controller 层返回值封装成统一格式 `RestWebResultContainer`
rest.enable-response-advice=true
```

### 2.5 统一返回值默认

## 3. 使用 `@Interceptor` 注解配置拦截器

```java
@Slf4j
@Order(1)
@Interceptor({"/foo/bar/**"})
public class FooInterceptor extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("preHandle : request {}", request.getRequestURI());
        if (StringUtils.isBlank(request.getParameter("param"))) {
            throw new InterceptException("deny");
        }
        return super.preHandle(request, response, handler);
    }
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        super.afterCompletion(request, response, handler, ex);
        log.info("afterCompletion : request {}", request.getRequestURI());
    }
}
```

## 4. 使用自定义注解拦截器

### 4.1 先定义一个注解

```java
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TestInterceptor {
}
```

### 4.2 编写拦截器（继承 `AbstractAnnotationHandlerInterceptorAdapter`）

```java
@Interceptor("/foo/wrap/**")
public class TestAnnotationHandlerInterceptor extends AbstractAnnotationHandlerInterceptorAdapter<TestInterceptor> {
    @Override
    protected Class<TestInterceptor> getRequiredAnnotationClass() {
        return TestInterceptor.class;
    }
    @Override
    protected Object before(HttpServletRequest request, HttpServletResponse response, Object handler, TestInterceptor anno) throws Exception {
        if (StringUtils.equals(request.getParameter("param"), anno.value())) {
            return true;
        } else {
            return "fail-data";
        }
    }
}
```

### 4.3 使用自定义注解拦截器

```java
@RestController
@RequestMapping("/foo")
//@TestInterceptor("val") // 如果注册到类上则拦截所有该方法
public class FooController {
    @GetMapping("/normal")
    public RestWebResultContainer normal() {
        return RestWebResults.succeed("normal");
    }
    @GetMapping("/bar")
    public RestWebResultContainer bar(String param, int num) {
        return RestWebResults.succeed("bar");
    }
    @GetMapping("/obj")
    public RestWebResultContainer obj(@RequestParam RestWebResultContainer container) {
        return container;
    }
    @GetMapping("/wrap")
    @TestInterceptor("val") // 如果注册到方法上则只拦截该方法
    public List<String> wrap() {
        return Collections.singletonList("wrap");
    }
    @GetMapping("/exception")
    public Object exception() throws Exception {
        throw new Exception("test");
    }
}
```
