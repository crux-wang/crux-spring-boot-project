/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.common.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.autoconfigure.AutoConfigurations;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.util.ResourceUtils;
import ren.crux.common.CommonAutoConfiguration;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Slf4j
public class ConfigurationParserTest {

    private ApplicationContextRunner contextRunner = new ApplicationContextRunner()
            .withConfiguration(AutoConfigurations.of(CommonAutoConfiguration.class, TestConfiguration.class));

    private ConfigurationParser configurationParser;
    private DefaultListableBeanFactory defaultListableBeanFactory;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void yamlBytes2Properties() {
        contextRunner.run(context -> {
            assertThat(context).hasSingleBean(ConfigurationParser.class);
            configurationParser = context.getBean(ConfigurationParser.class);
            URL url = ResourceUtils.getURL("src/test/resources/text.yml");
            byte[] bytes = IOUtils.toByteArray(url);
            assertNotNull(bytes);
            Properties properties = configurationParser.yamlBytes2Properties(bytes);
            System.out.println(properties);
            FooProperties foo = configurationParser.parse(FooProperties.class, properties).get();
            assertConfiguration(foo);
        });
    }

    @Test
    public void bytes2Properties() throws IOException {
        contextRunner.run(context -> {
            assertThat(context).hasSingleBean(ConfigurationParser.class);
            configurationParser = context.getBean(ConfigurationParser.class);
            Properties source = PropertiesLoaderUtils.loadAllProperties("text.properties");
            assertNotNull(source);
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            source.store(os, "");
            Properties desc = configurationParser.bytes2Properties(os.toByteArray());
            assertEquals(source, desc);
        });
    }

    @Test
    public void bind() {
        contextRunner.withPropertyValues("str=test").run(context -> {
            assertThat(context).hasSingleBean(ConfigurationParser.class);
            assertThat(context).hasSingleBean(FooProperties.class);
            configurationParser = context.getBean(ConfigurationParser.class);
            assertThat(configurationParser).isNotNull();
            defaultListableBeanFactory = (DefaultListableBeanFactory) context.getAutowireCapableBeanFactory();
            assertThat(defaultListableBeanFactory).isNotNull();
            FooProperties fooProperties = defaultListableBeanFactory.getBean(FooProperties.class);
            Properties properties = PropertiesLoaderUtils.loadAllProperties("text.properties");
            FooProperties foo = configurationParser.bind(fooProperties, properties).get();
            assertConfiguration(foo);
            assertEquals("test", foo.getStr());
            fooProperties = defaultListableBeanFactory.getBean(FooProperties.class);
            assertEquals(foo, fooProperties);
            assertEquals("test", context.getEnvironment().getProperty("foo.str"));
        });
    }

    @Test
    public void parse() throws IOException {
        contextRunner.run(context -> {
            configurationParser = context.getBean(ConfigurationParser.class);
            assertThat(configurationParser).isNotNull();
            Properties properties = PropertiesLoaderUtils.loadAllProperties("text.properties");
            FooProperties foo = configurationParser.parse(FooProperties.class, properties).get();
            assertConfiguration(foo);
        });
    }

    public void assertConfiguration(FooProperties foo) {
        assertEquals(123123L, foo.getLon());
        assertNotNull(foo.getStr());
        assertEquals(Arrays.asList("a", "b", "c", "d", "142"), foo.getList());
        Map<String, String> strMap = new HashMap<>();
        strMap.put("k1", "v1");
        strMap.put("k2", "v2");
        assertEquals(strMap, foo.getStrMap());
        Map<String, Bar> barMap = new HashMap<>();
        barMap.put("bk1", new Bar(1, "a"));
        barMap.put("bk2", new Bar(1, "123"));
        assertEquals(barMap, foo.getBarMap());
    }
}