/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.common.util;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.autoconfigure.AutoConfigurations;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;
import org.springframework.context.ApplicationContext;
import ren.crux.common.CommonAutoConfiguration;

public class ApplicationContextHelperTest {

    private ApplicationContextRunner contextRunner = new ApplicationContextRunner()
            .withConfiguration(AutoConfigurations.of(CommonAutoConfiguration.class));

    @Test
    public void isDebug() {
        this.contextRunner.run(context -> {
            ApplicationContext applicationContext = ApplicationContextHelper.getApplicationContext();
            Assert.assertNotNull(applicationContext);
            Assert.assertFalse(ApplicationContextHelper.isDebug());
        });
        this.contextRunner.withPropertyValues().withPropertyValues("debug=true").run(context -> {
            ApplicationContext applicationContext = ApplicationContextHelper.getApplicationContext();
            Assert.assertNotNull(applicationContext);
            Assert.assertTrue(ApplicationContextHelper.isDebug());
        });
    }

    @Test
    public void isProduction() {
        this.contextRunner.run(context -> {
            ApplicationContext applicationContext = ApplicationContextHelper.getApplicationContext();
            Assert.assertNotNull(applicationContext);
            Assert.assertTrue(ApplicationContextHelper.isProduction());
        });
        this.contextRunner.withPropertyValues().withPropertyValues("production=false").run(context -> {
            ApplicationContext applicationContext = ApplicationContextHelper.getApplicationContext();
            Assert.assertNotNull(applicationContext);
            Assert.assertFalse(ApplicationContextHelper.isProduction());
        });
    }

    @Test
    public void getApplicationContext() {
        this.contextRunner.run(context -> {
            ApplicationContext applicationContext = ApplicationContextHelper.getApplicationContext();
            Assert.assertNotNull(applicationContext);
        });
    }

}