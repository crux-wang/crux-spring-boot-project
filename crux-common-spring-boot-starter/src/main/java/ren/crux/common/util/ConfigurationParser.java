/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.common.util;

import lombok.NonNull;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConfigurationPropertiesBindHandlerAdvisor;
import org.springframework.boot.context.properties.bind.*;
import org.springframework.boot.context.properties.bind.handler.IgnoreErrorsBindHandler;
import org.springframework.boot.context.properties.bind.handler.IgnoreTopLevelConverterNotFoundBindHandler;
import org.springframework.boot.context.properties.bind.handler.NoUnboundElementsBindHandler;
import org.springframework.boot.context.properties.source.ConfigurationPropertySource;
import org.springframework.boot.context.properties.source.MapConfigurationPropertySource;
import org.springframework.boot.context.properties.source.UnboundElementsSourceFilter;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.validation.annotation.Validated;
import ren.crux.common.CommonConstants;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * Parse sources with Spring Boot Relaxed Binding 2.0
 * <p>
 * Warning : Not support {@link Validated}
 * <p>
 * {@link Binder}
 * <a href="https://github.com/spring-projects/spring-boot/wiki/Relaxed-Binding-2.0">Relaxed-Binding-2.0</a>
 *
 * @author wangzhihui
 */
public class ConfigurationParser {

    private final ApplicationContext applicationContext;
    private final MutablePropertySources propertySources;

    public ConfigurationParser(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
        this.propertySources = ((ConfigurableEnvironment) applicationContext.getEnvironment()).getPropertySources();
    }

    public Properties yamlResources2Properties(Resource... resources) {
        YamlPropertiesFactoryBean yamlPropertiesFactoryBean = new YamlPropertiesFactoryBean();
        yamlPropertiesFactoryBean.setResources(resources);
        return yamlPropertiesFactoryBean.getObject();
    }

    public Properties yamlBytes2Properties(byte[] bytes) {
        return yamlResources2Properties(new ByteArrayResource(bytes));
    }

    public Properties bytes2Properties(byte[] bytes) throws IOException {
        Properties properties = new Properties();
        Reader reader = new StringReader(new String(bytes, CommonConstants.UTF_8_CHARSET));
        properties.load(reader);
        return properties;
    }

    public <T> BindResult<@NonNull T> bind(@NonNull T instance, PlaceholdersResolver placeholdersResolver, ConfigurationPropertySource... sources) {
        ConfigurationProperties annotation = AnnotationUtils.findAnnotation(instance.getClass(), ConfigurationProperties.class);
        Binder binder = new Binder(Arrays.asList(sources), placeholdersResolver);
        Bindable<@NonNull T> target = Bindable.ofInstance(instance).withAnnotations(instance.getClass().getAnnotations());
        return binder.bind(Optional.ofNullable(annotation).isPresent() ? annotation.prefix() : "", target, getBindHandler(annotation));
    }

    public <T> BindResult<@NonNull T> bind(@NonNull T instance, Properties properties) {
        MapConfigurationPropertySource source = new MapConfigurationPropertySource(properties);
        propertySources.addLast(new PropertiesPropertySource("local", properties));
        PropertySourcesPlaceholdersResolver placeholdersResolver = new PropertySourcesPlaceholdersResolver(propertySources);
        return bind(instance, placeholdersResolver, source);
    }

    public <T> BindResult<T> parse(@NonNull Class<T> cls, PlaceholdersResolver placeholdersResolver, ConfigurationPropertySource... sources) {
        ConfigurationProperties annotation = AnnotationUtils.findAnnotation(cls, ConfigurationProperties.class);
        Binder binder = new Binder(Arrays.asList(sources), placeholdersResolver);
        Bindable<@NonNull T> target = Bindable.of(cls).withAnnotations(cls.getAnnotations());
        return binder.bind(Optional.ofNullable(annotation).isPresent() ? annotation.prefix() : "", target, getBindHandler(annotation));
    }

    public <T> BindResult<T> parse(@NonNull Class<T> cls, Properties properties) {
        MapConfigurationPropertySource source = new MapConfigurationPropertySource(properties);
        propertySources.addLast(new PropertiesPropertySource("local", properties));
        PropertySourcesPlaceholdersResolver placeholdersResolver = new PropertySourcesPlaceholdersResolver(propertySources);
        return parse(cls, placeholdersResolver, source);
    }

    private BindHandler getBindHandler(ConfigurationProperties annotation) {
        BindHandler handler = new IgnoreTopLevelConverterNotFoundBindHandler();
        if (annotation != null && annotation.ignoreInvalidFields()) {
            handler = new IgnoreErrorsBindHandler(handler);
        }
        if (annotation == null || !annotation.ignoreUnknownFields()) {
            UnboundElementsSourceFilter filter = new UnboundElementsSourceFilter();
            handler = new NoUnboundElementsBindHandler(handler, filter);
        }
        for (ConfigurationPropertiesBindHandlerAdvisor advisor : getBindHandlerAdvisors()) {
            handler = advisor.apply(handler);
        }
        return handler;
    }

    private List<ConfigurationPropertiesBindHandlerAdvisor> getBindHandlerAdvisors() {
        return this.applicationContext
                .getBeanProvider(ConfigurationPropertiesBindHandlerAdvisor.class)
                .orderedStream().collect(Collectors.toList());
    }

}