/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.common.util;


import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;

/**
 * Maybe you want to use application context in static methods.
 *
 * @author wangzhihui
 */
@Slf4j
public class ApplicationContextHelper {

    private static ApplicationContext applicationContext;
    private static boolean debug;
    private static boolean production;

    public static void set(ApplicationContext applicationContext) {
        ApplicationContextHelper.applicationContext = applicationContext;
        debug = applicationContext.getEnvironment().getProperty("debug", Boolean.class, false);
        production = applicationContext.getEnvironment().getProperty("production", Boolean.class, true);
    }

    public static boolean isDebug() {
        return debug;
    }

    public static boolean isProduction() {
        return production;
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }
}