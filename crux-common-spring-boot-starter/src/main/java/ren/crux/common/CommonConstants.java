/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.common;

import org.springframework.util.StringUtils;

import java.nio.charset.Charset;
import java.util.TimeZone;

/**
 * @author wangzhihui
 */
public interface CommonConstants {

    String UTF_8 = "UTF-8";

    Charset UTF_8_CHARSET = Charset.forName(UTF_8);

    String GMT_8 = "GMT+8";

    TimeZone GMT_8_TIME_ZONE = StringUtils.parseTimeZoneString(GMT_8);

    String SIMPLE_DATE_PATTERN = "yyyy-MM-dd";

    String SIMPLE_DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

}